﻿namespace Page_Replacement_algorithm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Nhap = new System.Windows.Forms.Button();
            this.Run = new System.Windows.Forms.Button();
            this.Mode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tBox = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tBox2 = new System.Windows.Forms.TextBox();
            this.tBox3 = new System.Windows.Forms.TextBox();
            this.TuNhap = new System.Windows.Forms.Button();
            this.atBox1 = new System.Windows.Forms.TextBox();
            this.atBox2 = new System.Windows.Forms.TextBox();
            this.atBox3 = new System.Windows.Forms.TextBox();
            this.Reset = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Nhap
            // 
            this.Nhap.BackColor = System.Drawing.Color.DodgerBlue;
            this.Nhap.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Nhap.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.HotTrack;
            this.Nhap.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.HotTrack;
            this.Nhap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Nhap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nhap.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Nhap.Location = new System.Drawing.Point(10, 40);
            this.Nhap.Name = "Nhap";
            this.Nhap.Size = new System.Drawing.Size(165, 28);
            this.Nhap.TabIndex = 0;
            this.Nhap.Text = "Nhập dữ liệu từ file";
            this.Nhap.UseVisualStyleBackColor = false;
            this.Nhap.Click += new System.EventHandler(this.Nhap_Click);
            // 
            // Run
            // 
            this.Run.BackColor = System.Drawing.Color.DodgerBlue;
            this.Run.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Run.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.HotTrack;
            this.Run.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.HotTrack;
            this.Run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Run.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Run.Image = ((System.Drawing.Image)(resources.GetObject("Run.Image")));
            this.Run.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Run.Location = new System.Drawing.Point(475, 69);
            this.Run.Name = "Run";
            this.Run.Size = new System.Drawing.Size(163, 56);
            this.Run.TabIndex = 1;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = false;
            this.Run.Click += new System.EventHandler(this.Run_Click);
            // 
            // Mode
            // 
            this.Mode.AccessibleDescription = "";
            this.Mode.AccessibleName = "";
            this.Mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Mode.FormattingEnabled = true;
            this.Mode.Items.AddRange(new object[] {
            "FIFO",
            "OPT",
            "LRU"});
            this.Mode.Location = new System.Drawing.Point(182, 9);
            this.Mode.Name = "Mode";
            this.Mode.Size = new System.Drawing.Size(180, 21);
            this.Mode.TabIndex = 2;
            this.Mode.Tag = "";
            this.Mode.SelectedIndexChanged += new System.EventHandler(this.Mode_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Lựa chọn giải thuật:";
            // 
            // tBox
            // 
            this.tBox.BackColor = System.Drawing.Color.OldLace;
            this.tBox.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.tBox.Location = new System.Drawing.Point(0, 233);
            this.tBox.Multiline = true;
            this.tBox.Name = "tBox";
            this.tBox.ReadOnly = true;
            this.tBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tBox.Size = new System.Drawing.Size(1122, 194);
            this.tBox.TabIndex = 4;
            this.tBox.WordWrap = false;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.textBox1.Location = new System.Drawing.Point(0, 205);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(1122, 22);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "- - - - - - - - - - KẾT QUẢ - - - - - - -\r\n - - -";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tBox2
            // 
            this.tBox2.BackColor = System.Drawing.SystemColors.Info;
            this.tBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tBox2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBox2.Location = new System.Drawing.Point(0, 433);
            this.tBox2.Name = "tBox2";
            this.tBox2.Size = new System.Drawing.Size(1122, 26);
            this.tBox2.TabIndex = 6;
            this.tBox2.TextChanged += new System.EventHandler(this.tBox2_TextChanged);
            // 
            // tBox3
            // 
            this.tBox3.Location = new System.Drawing.Point(181, 41);
            this.tBox3.Multiline = true;
            this.tBox3.Name = "tBox3";
            this.tBox3.ReadOnly = true;
            this.tBox3.Size = new System.Drawing.Size(181, 27);
            this.tBox3.TabIndex = 7;
            this.tBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tBox3.TextChanged += new System.EventHandler(this.tBox3_TextChanged);
            // 
            // TuNhap
            // 
            this.TuNhap.BackColor = System.Drawing.Color.DodgerBlue;
            this.TuNhap.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.TuNhap.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.HotTrack;
            this.TuNhap.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.HotTrack;
            this.TuNhap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TuNhap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TuNhap.Location = new System.Drawing.Point(95, 77);
            this.TuNhap.Name = "TuNhap";
            this.TuNhap.Size = new System.Drawing.Size(165, 28);
            this.TuNhap.TabIndex = 8;
            this.TuNhap.Text = "Tự nhập dữ liệu";
            this.TuNhap.UseVisualStyleBackColor = false;
            this.TuNhap.Click += new System.EventHandler(this.NhapTay_Click);
            // 
            // atBox1
            // 
            this.atBox1.Enabled = false;
            this.atBox1.Location = new System.Drawing.Point(184, 116);
            this.atBox1.Name = "atBox1";
            this.atBox1.Size = new System.Drawing.Size(181, 20);
            this.atBox1.TabIndex = 9;
            this.atBox1.Tag = "";
            this.atBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.atBox1.Click += new System.EventHandler(this.Click_TextChanged);
            this.atBox1.TextChanged += new System.EventHandler(this.atBox1_TextChanged);
            this.atBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // atBox2
            // 
            this.atBox2.Enabled = false;
            this.atBox2.Location = new System.Drawing.Point(184, 145);
            this.atBox2.Name = "atBox2";
            this.atBox2.Size = new System.Drawing.Size(181, 20);
            this.atBox2.TabIndex = 10;
            this.atBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.atBox2.Click += new System.EventHandler(this.Click1_TextChanged);
            this.atBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // atBox3
            // 
            this.atBox3.Enabled = false;
            this.atBox3.Location = new System.Drawing.Point(184, 174);
            this.atBox3.Name = "atBox3";
            this.atBox3.Size = new System.Drawing.Size(181, 20);
            this.atBox3.TabIndex = 11;
            this.atBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.atBox3.Click += new System.EventHandler(this.Click2_TextChanged);
            this.atBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // Reset
            // 
            this.Reset.BackColor = System.Drawing.Color.DodgerBlue;
            this.Reset.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Reset.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.HotTrack;
            this.Reset.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.HotTrack;
            this.Reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reset.Image = ((System.Drawing.Image)(resources.GetObject("Reset.Image")));
            this.Reset.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Reset.Location = new System.Drawing.Point(686, 69);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(149, 56);
            this.Reset.TabIndex = 12;
            this.Reset.Text = "&Reset ";
            this.Reset.UseVisualStyleBackColor = false;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.HotTrack;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.HotTrack;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(879, 69);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 56);
            this.button1.TabIndex = 13;
            this.button1.Text = "&Exit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Độ dài chuỗi tham chiếu:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 16);
            this.label3.TabIndex = 15;
            this.label3.Text = "Nhập vào số khung trang:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Nhập vào chuỗi tham chiếu:";
            // 
            // Form1
            // 
            this.AcceptButton = this.Run;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(1142, 474);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.atBox3);
            this.Controls.Add(this.atBox2);
            this.Controls.Add(this.atBox1);
            this.Controls.Add(this.TuNhap);
            this.Controls.Add(this.tBox3);
            this.Controls.Add(this.tBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Mode);
            this.Controls.Add(this.Run);
            this.Controls.Add(this.Nhap);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "FIFO OPT LRU";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Nhap;
        private System.Windows.Forms.Button Run;
        private System.Windows.Forms.ComboBox Mode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tBox2;
        private System.Windows.Forms.TextBox tBox3;
        private System.Windows.Forms.Button TuNhap;
        private System.Windows.Forms.TextBox atBox1;
        private System.Windows.Forms.TextBox atBox2;
        private System.Windows.Forms.TextBox atBox3;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

