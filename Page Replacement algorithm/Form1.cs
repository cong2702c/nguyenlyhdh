﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Page_Replacement_algorithm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public  int[] frames = new int[1000];
        public  int[] ffr = new int[1000];
        public  int[] time = new int[1000];
        public  int[] a = new int[1000];
        public  int frame, n, count=0;
        public  int[,] fra = new int[100,1000];
        public  char[]    pf = new char[1000];
        public  int choice;
        public  bool tieptuc;

        private void ReadFile()
        {
            int i=0;
            FileStream fi = new FileStream("test.txt", FileMode.Open, FileAccess.Read);
            StreamReader rd = new StreamReader(fi);

            string line = null;
            string[] tg = null;
            line = rd.ReadLine();
            tg = new Regex(" ").Split(line);
            if (tg[0] == null || tg[1] == null)
            {
                MessageBox.Show("Sai dữ liệu vào!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                reset();
            }
            else
            {
                n = int.Parse(tg[0]);
                frame = int.Parse(tg[1]);
                a[1] = int.Parse(tg[1]);
                while ((line = rd.ReadLine()) != null)
                {
                    tg = new Regex(" ").Split(line);
                    int k = 0;
                    foreach (string subtg in tg)
                    {
                        i++;
                        a[i] = int.Parse(tg[k]);
                        k++;
                    }
                }
                if (i < n)
                {
                    MessageBox.Show("Sai dữ liệu vào!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    reset();
                }
                else
                    tBox3.Text = "Lấy dữ liệu thành công !";
            }
            fi.Close();
        }

        private void Nhap_Click(object sender, EventArgs e)
        {
            TuNhap.Enabled = false;
            ReadFile();
        }

        void khoitao()
        {
            count = 0;
            pf = new char[1000];
            frames = new int[1000];
            ffr = new int[1000];
            time = new int[1000];
            fra = new int[100, 1000];
            tBox2.ResetText();
        }

        private void Run_Click(object sender, EventArgs e)
        {
            khoitao();
            if (Nhap.Enabled == true)
                Thuchien();
            else
            {
                Read1();
                if (tieptuc == true)
                {
                    Thuchien1();
                    atBox1.ReadOnly = true;
                    atBox2.ReadOnly = true;
                    atBox3.ReadOnly = true;
                }
                else
                    reset();
            }
        }

        void FIFO()
        {
            int i, j, k, available;
            for (i = 0; i < frame; i++)
                frames[i] = -1;
            j = 0;
            for (i = 1; i <= n; i++)
            {
                available = 0;
                for (k = 0; k < frame; k++)
                    if (frames[k] == a[i])
                    {
                        pf[i] = ' ';
                        available = 1;
                    }
                if (available == 0)
                {
                    frames[j] = a[i];
                    pf[i] = '*';
                    j = (j + 1) % frame;
                    count++;
                    for (k = 0; k < frame; k++)
                        fra[k,i] = frames[k];
                }
                else
                {
                    for (k = 0; k < frame; k++)
                        fra[k,i] = frames[k];
                }

            }
        }

        int find(int x, int j)
        {
            int i;
            if (j == n) return 0;
            for (i = j; i <= n; i++)
                if (a[i] == x)
                    return i;
            return 0;
        }

        void OPT()
        {
            int i, j, k, available, vt, temp;
            for (i = 0; i < frame; i++)
                frames[i] = -1;
            j = 0;
            for (i = 1; i <= n; i++)
            {
                available = 0;
                for (k = 0; k < frame; k++)
                    if (frames[k] == a[i])
                    {
                        available = 1;
                        pf[i] = ' ';
                    }
                if (available == 0)
                {
                    frames[j] = a[i];
                    count++;
                    for (k = 0; k < frame; k++)
                        fra[k,i] = frames[k];
                    pf[i] = '*';
                }
                else
                {
                    for (k = 0; k < frame; k++)
                        fra[k,i] = frames[k];
                }
                temp = 0;
                for (k = 0; k < frame; k++)
                {
                    vt = find(frames[k], i + 1);
                    if (vt == 0)
                    {
                        j = k;
                        break;
                    }
                    else
                        if (vt > temp)
                        {
                            j = k;
                            temp = vt;
                        }
                }
            }
        }

        void LRU()
        {
            int i, j, k, available;
            for (i = 0; i < frame; i++)
            {
                frames[i] = -1;
                time[i] = 0;
            }
            j = 0;
            for (i = 1; i <= n; i++)
            {
                available = 0;
                for (k = 0; k < frame; k++)
                    if (frames[k] == a[i])
                    {
                        available = 1;
                        time[k] = i;
                        pf[i] = ' ';
                    }
                if (available == 0)
                {
                    frames[j] = a[i];
                    time[j] = i;
                    count++;
                    for (k = 0; k < frame; k++)
                        fra[k,i] = frames[k];
                    pf[i] = '*';
                }
                else
                {
                    for (k = 0; k < frame; k++)
                        fra[k,i] = frames[k];
                }
                for (k = 0; k < frame; k++)
                    if (time[k] < time[j])
                        j = k;
            }
        }

        void init()
        {
            int i, j;
            for (i = 1; i <= n; i++)
            {
                for (j = 0; j < frame; j++)
                    fra[j,i] = -1;
                pf[i] = ' ';
            }
        }

        private void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != ' ')
            {
                e.Handled = true;
                MessageBox.Show("Không được phép nhập kí tự!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void Thuchien()
        {
            int i, j;
            if (choice == 0)
                MessageBox.Show("Chưa chọn giải thuật thay trang!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                if (choice == 1)
                    FIFO();
                else
                    if (choice == 2)
                        OPT();
                    else
                        if (choice == 3)
                            LRU();
                tBox.Text = "";
                for (i = 1; i <= n; i++)
                {
                    tBox.Text += a[i].ToString();
                    tBox.Text += "\t";
                }
                tBox.Text += Environment.NewLine;
                for (i = 0; i < frame; i++)
                {
                    for (j = 1; j <= n; j++)
                        if (fra[i, j] != -1)
                            tBox.Text += fra[i, j].ToString() + "\t";
                        else
                            tBox.Text += "\t";
                    tBox.Text += Environment.NewLine;
                }
                for (i = 1; i <= n; i++)
                    tBox.Text += pf[i].ToString() + "\t";
                tBox2.Text += " => Số trang lỗi là: ";
                tBox2.Text += count.ToString();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        private void NhapTay_Click(object sender, EventArgs e)
        {
            Nhap.Enabled = false;
            atBox1.Enabled = true;
            atBox2.Enabled = true;
            atBox3.Enabled = true;
            atBox1.ReadOnly = false;
            atBox2.ReadOnly = false;
            atBox3.ReadOnly = false;
        }

        void Read1()
        {
            int i;
            string[] tg = null;
            tg = new Regex(" ").Split(atBox3.Text);
            if (atBox1.Text == "" || atBox1.Text == "Nhập số trang" || atBox2.Text == "" || atBox2.Text == "Nhập số khung trang" || atBox3.Text == "Nhập chuỗi trang")
            {
                MessageBox.Show("Chưa nhập dữ liệu hoặc nhập dữ liệu sai!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tieptuc = false;
            }
            else
            {
                tieptuc = true;
                n = int.Parse(atBox1.Text);
                if (tg.Count() == n)
                {
                    frame = int.Parse(atBox2.Text);
                    for (i = 1; i <= n; i++)
                        a[i] = int.Parse(tg[i - 1]);
                    tBox3.Text = "Nhập dữ liệu thành công";
                }
                else
                {
                    MessageBox.Show("Chưa nhập dữ liệu hoặc nhập dữ liệu sai!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tieptuc = false;
                }
            }
        }

        void Thuchien1()
        {
            int i, j;
            if (choice == 0)
                MessageBox.Show("Chưa chọn giải thuật thay trang!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                if (choice == 1)
                    FIFO();
                else
                    if (choice == 2)
                        OPT();
                    else
                        if (choice == 3)
                            LRU();
                tBox.Text = "";
                for (i = 1; i <= n; i++)
                {
                    tBox.Text += a[i].ToString();
                    tBox.Text += "\t";
                }
                tBox.Text += Environment.NewLine;
                for (i = 0; i < frame; i++)
                {
                    for (j = 1; j <= n; j++)
                        if (fra[i, j] != -1)
                            tBox.Text += fra[i, j].ToString() + "\t";
                        else
                            tBox.Text += "\t";
                    tBox.Text += Environment.NewLine;
                }
                for (i = 1; i <= n; i++)
                    tBox.Text += pf[i].ToString() + "\t";
                tBox2.Text += " => Số trang lỗi là: ";
                tBox2.Text += count.ToString();
            }
        }

        bool Nhaplai1 = true;
        private void Click_TextChanged(object sender, EventArgs e)
        {
            if (Nhaplai1 == true)
                atBox1.Text = "";
            Nhaplai1 = false;
        }
        bool Nhaplai2 = true;
        private void Click1_TextChanged(object sender, EventArgs e)
        {
            if (Nhaplai2 == true)
                atBox2.Text = "";
            Nhaplai2 = false;
        }
        bool Nhaplai3 = true;
        private void Click2_TextChanged(object sender, EventArgs e)
        {
            if (Nhaplai3 == true)
                atBox3.Text = "";
            Nhaplai3 = false;
        }

        private void Mode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Mode.Text == "FIFO")
                choice = 1;
            if (Mode.Text == "OPT")
                choice = 2;
            if (Mode.Text == "LRU")
                choice = 3;
        }

        void reset()
        {
            Nhap.Enabled = true;
            TuNhap.Enabled = true;
            atBox1.Enabled = false;
            atBox2.Enabled = false;
            atBox3.Enabled = false;
            khoitao();
            tBox.ResetText();
            tBox2.ResetText();
            tBox3.ResetText();
            //atBox1.Text = "Nhập số trang";
            //atBox2.Text = "Nhập số khung trang";
            //atBox3.Text = "Nhập chuỗi trang";
            choice = 0;
            Mode.Items.Clear();
            Mode.Items.Add("FIFO");
            Mode.Items.Add("OPT");
            Mode.Items.Add("LRU"); 
            pf = new char[1000];
            frames = new int[1000];
            ffr = new int[1000];
            time = new int[1000];
            a = new int[1000];
            count=0;
            fra = new int[100,1000];
        }

        private void atBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void tBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Reset_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
